<?php

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

/*
if (!Configure::read('debug')):
    throw new NotFoundException('');
endif;
*/

$cakeDescription = 'TimeBank app';
?>

<!DOCTYPE html>
<html>

    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>
        </title>

        <?= $this->Html->meta('icon') ?>
        <?= $this->Html->css('foundation.min.css') ?>
        <!--
        <?= $this->Html->css('base.css') ?>
        <?= $this->Html->css('cake.css') ?>
        <?= $this->Html->css('home.css') ?>
        -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
    </head>

    <body class="home">
        <code>simple notLaravel CRM</code>
        <form>
            <div class="medium-4 columns">
                <h2>Login</h2>
                <div class="">
                <input type="text" placeholder="login">
                </div>
                <div class="">
                <input type="text" placeholder="password">
                </div>
                <a href="#" class="button">Let me in</a>
            </div>
        </form>
    </body>
</html>
