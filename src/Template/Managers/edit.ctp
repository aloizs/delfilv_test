<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $manager->ManagerID],
                ['confirm' => __('Are you sure you want to delete # {0}?', $manager->ManagerID)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Managers'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="managers form large-9 medium-8 columns content">
    <?= $this->Form->create($manager) ?>
    <fieldset>
        <legend><?= __('Edit Manager') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('LastName');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
