CREATE TABLE managers (
    ManagerID INT AUTO_INCREMENT PRIMARY KEY,
    Name VARCHAR(30),
    LastName VARCHAR(30),
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created DATETIME,
    modified DATETIME
);

CREATE TABLE clients (
    ClientID INT AUTO_INCREMENT PRIMARY KEY,
    ClientTitle VARCHAR(50),
    PhoneN NUMERIC,
    RegN NUMERIC NOT NULL,
    password VARCHAR(255) NOT NULL,
    created DATETIME,
    modified DATETIME
);

CREATE TABLE orders (
    OrderID INT AUTO_INCREMENT PRIMARY KEY,
    OrderTitle VARCHAR(50),
    Description TEXT,
    Price DECIMAL(10,2),
    created DATETIME,
    modified DATETIME
);
